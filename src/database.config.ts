//export default () => ({})：這是一個默認導出的箭頭函式。它會返回一個對象，其中包含 MongoDB 連接的配置資訊。
export default () => ({
  //database：這是對象的屬性，表示 MongoDB 連接的配置。
  database: {
    //host：這是 database 對象的屬性，表示 MongoDB 伺服器的主機名稱或 IP 位址。它會從環境變數中讀取 DB_HOST，如果未定義則使用預設值 'localhost'。
    host: process.env.DB_HOST || 'localhost',
    //port：這是 database 對象的屬性，表示 MongoDB 伺服器的連接埠號。它會從環境變數中讀取 DB_PORT，如果未定義或無效，則使用預設值 27017。
    port: parseInt(process.env.DB_PORT, 10) || 27017,
    //name：這是 database 對象的屬性，表示要連接的資料庫名稱。它會從環境變數中讀取 DB_NAME，如果未定義則使用預設值 'your_database_name'。
    name: process.env.DB_NAME || 'your_database_name',
  },
});

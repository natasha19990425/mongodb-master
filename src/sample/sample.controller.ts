import { Controller, Get } from '@nestjs/common';
import { SampleService } from './sample.service';
import { Company } from '../interface';
import { Bag } from '../interface';
import { Item } from '../interface';
import { ItemType } from '../interface';

@Controller('sample')
export class SampleController {
  constructor(private readonly sampleService: SampleService) {}

  //定義了一個GET路由處理程序，對應到/company-info路徑。
  //當收到該請求時，Nest.js會調用getCompanyInfo方法並返回一個Promise，其中包含Company類型的陣列。
  @Get('/company-info')
  async getCompanyInfo(): Promise<Company[]> {
    return this.sampleService.getCompanyInfo();
  }

  //定義了一個GET路由處理程序，對應到/company-options路徑。
  //當收到該請求時，Nest.js會調用getCompanyOptions方法並返回一個Promise，其中包含Company類型的陣列。
  @Get('/company-options')
  async getCompanyOptions(): Promise<Company[]> {
    return this.sampleService.getCompanyOptions();
  }

  //定義了一個GET路由處理程序，對應到/item-options路徑。
  //當收到該請求時，Nest.js會調用getItemOptions方法並返回一個Promise，其中包含Item類型的陣列。
  @Get('/item-options')
  async getItemOptions(): Promise<Item[]> {
    return this.sampleService.getItemOptions();
  }

  //定義了一個GET路由處理程序，對應到/bag-options路徑。
  //當收到該請求時，Nest.js會調用getBagOptions方法並返回一個Promise，其中包含Bag類型的陣列。
  @Get('/bag-options')
  async getBagOptions(): Promise<Bag[]> {
    return this.sampleService.getBagOptions();
  }

  //定義了一個GET路由處理程序，對應到/item-type路徑。
  //當收到該請求時，Nest.js會調用getItemType方法並返回一個Promise，其中包含ItemType類型的陣列。
  @Get('/item-type')
  async getItemType(): Promise<ItemType[]> {
    return this.sampleService.getItemType();
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { SampleController } from './sample.controller';
import { SampleService } from './sample.service';

describe('AppController', () => {
  let appController: SampleController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [SampleController],
      providers: [SampleService],
    }).compile();

    appController = app.get<SampleController>(SampleController);
  });

  describe('root', () => {
    it('should return "Hello getCompanyInfo!"', () => {
      expect(appController.getCompanyInfo()).toBe('Hello getCompanyInfo!');
      expect(appController.getCompanyOptions()).toBe(
        'Hello getCompanyOptions!',
      );
      expect(appController.getItemOptions()).toBe('Hello getItemOptions!');
      expect(appController.getItemType()).toBe('Hello getItemType!');
      expect(appController.getBagOptions()).toBe('Hello getSetOptions!');
    });
  });
});

import { Injectable } from '@nestjs/common';
import { MongoClient, Db, Collection } from 'mongodb';
import { ConfigService } from '@nestjs/config';
import { Company } from '../interface';
import { Bag } from '../interface';
import { Item } from '../interface';
import { ItemType } from '../interface';
import { MongodbService } from 'src/mongodb/mongodb/src';

@Injectable()
export class SampleService {
  private client: MongoClient;
  private db: Db;

  constructor(
    private configService: ConfigService,
    private mongodbService: MongodbService,
  ) {}

  ngOnInit(): void {
    this.client = this.mongodbService.getClient();
    this.db = this.mongodbService.getDatabase();
    // 在此使用 client 和 db 進行 MongoDB 操作
  }

  //每個數據獲取方法中使用getCollection方法獲取相應的集合，並使用.find().toArray()操作獲取該集合中的所有數據。
  private getCollection<T>(collectionName: string): Collection<T> {
    return this.db.collection<T>(collectionName);
  }

  async getCompanyInfo(): Promise<Company[]> {
    //當調用getCompanyInfo方法時，將返回對應集合中的數據。
    const collection = this.getCollection<Company>('companies');
    const result = await collection.find().toArray();
    return result;
  }

  async getCompanyOptions(): Promise<Company[]> {
    //當調用getCompanyOptions方法時，將返回對應集合中的數據。
    const collection = this.getCollection<Company>('companyOptions');
    const result = await collection.find().toArray();
    return result;
  }

  async getItemOptions(): Promise<Item[]> {
    //當調用getItemOptions方法時，將返回對應集合中的數據。
    const collection = this.getCollection<Item>('itemOptions');
    const result = await collection.find().toArray();
    return result;
  }

  async getBagOptions(): Promise<Bag[]> {
    //當調用getBagOptions方法時，將返回對應集合中的數據。
    const collection = this.getCollection<Bag>('bagOptions');
    const result = await collection.find().toArray();
    return result;
  }

  async getItemType(): Promise<ItemType[]> {
    //當調用getItemType方法時，將返回對應集合中的數據。
    const collection = this.getCollection<ItemType>('itemTypes');
    const result = await collection.find().toArray();
    return result;
  }
}

//我們將 ConfigModule 設置為全局模組，以便我們可以在整個應用程序中訪問配置。
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SampleController } from './sample/sample.controller';
import { SampleService } from './sample/sample.service';

@Module({
  imports: [
    //這是 NestJS 提供的配置模組，用於處理應用程序的配置。我們使用它來載入和設定環境變數。
    ConfigModule.forRoot({
      //這是 ConfigModule 的配置選項之一。它指示該模組的配置在整個應用程序中是全局的，可以在其他模組中使用。
      isGlobal: true,
    }),
  ],
  controllers: [SampleController],
  providers: [SampleService, ConfigService],
})
export class AppModule {}

// company.model.ts
export interface Company {
  id: string;
  name: string;
  address: string;
}

// item.model.ts
export interface Item {
  id: string;
  name: string;
}

// bag.model.ts
export type Bag = Item;

// item-type.model.ts
export type ItemType = Item;
